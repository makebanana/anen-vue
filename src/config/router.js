import Home from '@/pages/Home'
import BtnGroup from '@/pages/BtnGroup'
import Pagination from '@/pages/Pagination'

const routes = [{
  path: '/',
  component: Home
}, {
  path: '/btngroup',
  component: BtnGroup
}, {
  path: '/pagination',
  component: Pagination
}]

export {routes};
